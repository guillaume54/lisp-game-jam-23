# Magnetic Trek

This is my entry for the [itch.io spring lisp game jam of
2023](https://itch.io/jam/spring-lisp-game-jam-2023). The game was
written in [Chicken Scheme](http://www.call-cc.org/) with SDL.

## How to play

The game is a top-down scroller where the player controls a charged
particle and must consume as many other charged particles as
possible. There are black obstacles that must be avoided as touching
them is instant death!

Each sector (stage) completed increases your point total. But beware,
speed increases and the gaps narrow down as you progress.

## Dependencies and binary

The game comes with a statically-compiled binary for x86_64 linux
systems packaged as `magtrek-linux-amd64.zip`. In order to run it,
decompress it somewhere and install the following dependencies:

- `SDL2`
- `SDL2_ttf`

## Command-line options

The following command line options are available to help with
rendering issues:

- `-vsync` to synchronise the video output to the monitor
- `-software` to use the software renderer (non-accelerated).

## Compiling and dependencies

In order to install the game from source, Make sure you have chicken
5, SDL2 and SDL2_ttf all installed, then run (potentially with sudo):

```
make deps
```

This will install the necessary chicken modules. Then run the game
with:

```
make run
```
