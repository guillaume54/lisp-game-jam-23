(module trek
    ()
  (import scheme
          (prefix sdl2 "sdl2:")
          (prefix sdl2-ttf "ttf:")
          (chicken base)
          (chicken condition)
          (chicken format)
          (chicken random)
          (chicken process-context) ; For command-line args
          (srfi 1)
          miscmacros
          defstruct)

  (define +color-depth+ 32)
  (define +base-particle-size+ 10)
  (define +obstacle-width+ 50)

  (defstruct particle
    id
    position
    (enabled #t)
    (prev-position '())
    (dirty #t)
    (polarity 1)
    (magnitude 1)
    (surface '()))

  (define (particle-disable! particle)
    (particle-enabled-set! particle #f))

  (define (particle-enable! particle)
    (particle-enabled-set! particle #t))

  (define (particle-magnitude-inc! particle)
    "Increase the magnitue of PARTICLE"
    (particle-magnitude-set! particle (+ 1 (particle-magnitude particle))))

  (define (particle-magnitude-dec! particle)
    "Decrease the magnitue of PARTICLE"
    (particle-magnitude-set! particle (- 1 (particle-magnitude particle))))

  (define (particle-x particle)
    (car (particle-position particle)))

  (define (particle-y particle)
    (cdr (particle-position particle)))

  (define (particle-size particle)
    (+ +base-particle-size+
       (* 3 (particle-magnitude particle))))

  (define (particle-height particle)
    (particle-size particle))

  (define (particle-width particle)
    (particle-size particle))

  (define (particle-color particle)
    (let ((polarity (particle-polarity particle)))
      (cond ((eq? polarity 1)
             (sdl2:make-color 255 0 0)) ; Red
            ((eq? polarity -1)
             (sdl2:make-color 0 0 255))))) ; Blue

  (define (particle-prev-rect particle)
    (if (null? (particle-prev-position particle))
        '()
        (sdl2:make-rect (car (particle-prev-position particle))
                        (cdr (particle-prev-position particle))
                        (particle-height particle)
                        (particle-width particle))))

  (define (particle-rect particle)
    (sdl2:make-rect (particle-x particle)
                    (particle-y particle)
                    (particle-height particle)
                    (particle-width particle)))

  (define (particle-move! particle dx dy)
    (let* ((position (particle-position particle))
           (x (car position))
           (y (cdr position)))
      (particle-prev-position-set! particle position)
      (particle-position-set! particle (cons (+ x dx) (+ y dy)))
      (particle-dirty-set! particle #t)))

  (define (particle-mag-range particle)
    (* 100 (log (particle-magnitude particle))))


  (defstruct obstacle
    id
    position
    (gap-start 0)
    (gap-end 0)
    (prev-position '())
    (dirty #t)
    (polarity 0)
    (enabled #t))

  (define (obstacle-prev-rects obstacle)
    (if (null? (obstacle-prev-position obstacle))
        '()
        (list
         (sdl2:make-rect (obstacle-prev-position obstacle) 0
                         +obstacle-width+ (obstacle-gap-start obstacle))
         (sdl2:make-rect (obstacle-prev-position obstacle) (obstacle-gap-end obstacle)
                         +obstacle-width+ (- +screen-height+ (obstacle-gap-end obstacle))))))

  (define (obstacle-rects obstacle)
    (list
     (sdl2:make-rect (obstacle-position obstacle) 0
                     +obstacle-width+ (obstacle-gap-start obstacle))
     (sdl2:make-rect (obstacle-position obstacle) (obstacle-gap-end obstacle)
                     +obstacle-width+ (- +screen-height+ (obstacle-gap-end obstacle)))))


  (define (obstacle-move! obstacle dx)
    (let ((position (obstacle-position obstacle)))
      (obstacle-prev-position-set! obstacle position)
      (obstacle-position-set! obstacle (+ position dx))
      (obstacle-dirty-set! obstacle #t)))


  (define (obstacle-disable! obstacle)
    (obstacle-enabled-set! obstacle #f))

  (define (obstacle-enable! obstacle)
    (obstacle-enabled-set! obstacle #t))

  (define (obstacle-randomize! obstacle position min-gap max-gap)
    (let* ((gap-size (+ min-gap (pseudo-random-integer (- max-gap min-gap))))
           (gap-start (pseudo-random-integer (- +screen-height+ gap-size)))
           (gap-end (+ gap-start gap-size)))

      (obstacle-prev-position-set! obstacle position)
      (obstacle-position-set! obstacle (+ +screen-width+ position))
      (obstacle-dirty-set! obstacle #t)

      (obstacle-gap-start-set! obstacle gap-start)
      (obstacle-gap-end-set! obstacle gap-end)
      obstacle))

  (define (make-random-obstacle id position min-gap max-gap)
    (let ((obstacle (make-obstacle id: id position: 0 gap-start: 0 gap-end: 0)))
      (obstacle-randomize! obstacle position min-gap max-gap)))

  (define +screen-width+ 600)
  (define +screen-height+ 400)
  (define *fullscreen?* #f)
  (define +background-color+ (sdl2:make-color 241 135 82))
  (define +software-mode?+ (member "-software" (command-line-arguments)))
  (define +vsync?+ (member "-vsync" (command-line-arguments)))

  (define (main)
    (sdl2:set-main-ready!)
    (sdl2:init! '(video))
    (ttf:init!)

    (on-exit sdl2:quit!)


    ;; Install a custom exception handler that will call quit! and then
    ;; call the original exception handler. This ensures that quit! will
    ;; be called even if an unhandled exception reaches the top level.
    (current-exception-handler
     (let ((original-handler (current-exception-handler)))
       (lambda (exception)
         (sdl2:quit!)
         (original-handler exception))))

    (if (and +vsync?+ (not +software-mode?+)) ; vsync doesn't work with software rendering
        (sdl2:set-hint! 'render-vsync "1"))
    (define *window* (sdl2:create-window! "Magnetic Trek"
                                          'centered 'centered
                                          +screen-width+ +screen-height+
                                          (if *fullscreen?* '(fullscreen) '())))
    (define *renderer* (sdl2:create-renderer! *window* -1
                                              (if +software-mode?+ '(software) '(accelerated))))
    
    (define *font* (ttf:open-font "DejaVuSans.ttf" 12))
    (define *text-color* (sdl2:make-color 0 0 0))

    (set! (sdl2:render-draw-color *renderer*) +background-color+)
    (sdl2:render-clear! *renderer*)

    (set! (sdl2:render-viewport *renderer*)
      (sdl2:make-rect 0 0 +screen-width+ +screen-height+))


    (define (random-position)
      (cons (pseudo-random-integer +screen-width+)
            (pseudo-random-integer +screen-height+)))

    (define (random-polarity)
      (if (> 50 (pseudo-random-integer 100)) 1 -1))

    (define (scene-make-particles-list number)
      (if (> number 0)
          (cons (cons (random-position) (random-polarity))
                (scene-make-particles-list (- number 1)))
          '()))

    (define scene (list (cons 'player (cons (inexact->exact (/ +screen-width+ 2))
                                            (inexact->exact (/ +screen-height+ 2))))
                        (cons 'particles (scene-make-particles-list (+ 5 (pseudo-random-integer 6))))))

    (define (draw-particle! renderer particle)
      "Blit one particle to the main window"
      (if (particle-dirty particle)
          (let ((prev-rect (particle-prev-rect particle))
                (rect (particle-rect particle)))
            (if (not (null? prev-rect))
                (begin
                  ;; Erase the previous particle's spot
                  (sdl2:render-draw-color-set! renderer +background-color+)
                  (sdl2:render-fill-rect! renderer prev-rect)))

            (if (particle-enabled particle)
                (begin
                  ;; Draw the current particle
                  (sdl2:render-draw-color-set! renderer (particle-color particle))
                  (sdl2:render-fill-rect! renderer rect)))
            (particle-dirty-set! particle #f))))

    (define (draw-obstacle! renderer obstacle)
      "Blit one obstacle to the main window"
      (if (obstacle-dirty obstacle)
          (let ((prev-rects (obstacle-prev-rects obstacle))
                (rects (obstacle-rects obstacle)))
            (if (not (null? prev-rects))
                (begin
                  ;; Erase the previous obstacle's spots
                  (sdl2:render-draw-color-set! renderer +background-color+)
                  (for-each (lambda (rect) (sdl2:render-fill-rect! renderer rect))
                            prev-rects)))

            (if (obstacle-enabled obstacle)
                (begin
                  ;; Draw the current obstacle
                  (sdl2:render-draw-color-set! renderer (sdl2:make-color 0 0 0))
                  (for-each (lambda (rect) (sdl2:render-fill-rect! renderer rect))
                            rects)))
            (obstacle-dirty-set! obstacle #f))))

    (define (draw-particles! renderer particles)
      "Blit a list of particles to the main window"
      (if (null? particles)
          particles
          (let ((particle (car particles))
                (rest (cdr particles)))
            (draw-particle! renderer particle)
            (draw-particles! renderer rest))))

    (let* ((player (make-particle id: "player" position: (cdr (assoc 'player scene)) magnitude: 2))
           (particles (fold
                       (lambda (parameters all-particles)
                         (cons (make-particle
                                id: (format "particle-~S" (length all-particles))
                                position: (car parameters)
                                polarity: (cdr parameters))
                               all-particles))
                       '()
                       (cdr (assoc 'particles scene))))
           ;; Only use 3 obstacles and rotate them on and on.
           (obstacle-min-gap 100)
           (obstacle-max-gap 300)
           (obstacles '())
           (obstacle-speed -1)
           (obstacle-num 5)
           (event (sdl2:make-event))
           (pressed-keys '())
           (stage-num 1)
           (game-points 0))

      (define (kbd-event-match? event event-type kbd-sym)
        (and (sdl2:keyboard-event? event)
             (eq? (sdl2:event-type event) event-type)
             (if (null? kbd-sym)
                 #t
                 (eq? (sdl2:keyboard-event-sym event) kbd-sym))))

      (define (key-up? event sym)
        (kbd-event-match? event 'key-up sym))

      (define (key-down? event sym)
        (kbd-event-match? event 'key-down sym))


      (define (handle-event event exitor!)
        ;; (display event) ;; Logging events
        ;; (display "\n")
        (if event
            (begin
              (cond ((key-down? event 'escape)
                     (exitor! #t))
                    ((key-up? event 'space)
                     (particle-polarity-set! player (* -1 (particle-polarity player))))

                    ((key-down? event 'w)
                     (set! pressed-keys (cons 'up pressed-keys)))
                    ((key-up? event 'w)
                     (set! pressed-keys (delete 'up pressed-keys)))

                    ((key-down? event 'a)
                     (set! pressed-keys (cons 'left pressed-keys)))
                    ((key-up? event 'a)
                     (set! pressed-keys (delete 'left pressed-keys)))

                    ((key-down? event 's)
                     (set! pressed-keys (cons 'down pressed-keys)))
                    ((key-up? event 's)
                     (set! pressed-keys (delete 'down pressed-keys)))

                    ((key-down? event 'd)
                     (set! pressed-keys (cons 'right pressed-keys)))
                    ((key-up? event 'd)
                     (set! pressed-keys (delete 'right pressed-keys)))))))


      (define (display-text! message centered?)
        (let*-values (((points-w points-h) (ttf:size-text *font* message))
                      ((points) (ttf:render-text-solid *font* message *text-color*))
                      ((points-texture) (sdl2:create-texture-from-surface *renderer* points))
                      ((dest-rect) (sdl2:make-rect
                                    (if centered?
                                        (inexact->exact (round (/ (- +screen-width+ points-w) 2)))
                                        0)
                                    (if centered?
                                        (inexact->exact (round (/ (- +screen-height+ points-h) 2)))
                                        0)
                                    points-w
                                    points-h)))
          (sdl2:render-copy! *renderer* points-texture #f dest-rect)))

      (define (display-line! message y-offset)
        (let*-values (((points-w points-h) (ttf:size-text *font* message))
                      ((points) (ttf:render-text-solid *font* message *text-color*))
                      ((points-texture) (sdl2:create-texture-from-surface *renderer* points))
                      ((dest-rect) (sdl2:make-rect
                                    0
                                    y-offset
                                    points-w
                                    points-h)))
          (sdl2:render-copy! *renderer* points-texture #f dest-rect)))

      (define (dx-to-player particle)
        (let* ((x (particle-x particle))
               (player-x (particle-x player))
               (delta-x (- player-x x))
               (direction (if (eq? (particle-polarity player)
                                   (particle-polarity particle)) -1 +1))
               (movement (if (zero? delta-x)
                             delta-x
                             (* direction (/ delta-x (abs delta-x))))))
          (if (eq? movement -1) ; Amplify rear movement to compensate for the scene motion
              -2
              movement)))

      (define (dy-to-player particle)
        (let* ((y (particle-y particle))
               (player-y (particle-y player))
               (delta-y (- player-y y))
               (direction (if (eq? (particle-polarity player) (particle-polarity particle)) -1 +1)))
          (if (zero? delta-y)
              delta-y
              (* direction (/ delta-y (abs delta-y))))))

      (define (distance-to-player particle)
        (let ((x (particle-x particle))
              (player-x (particle-x player))
              (y (particle-y particle))
              (player-y (particle-y player)))
          (sqrt (+ (expt (- player-x x) 2)
                   (expt (- player-y y) 2)))))

      (display-line! "Move your particle with W,A,S,D" 0)
      (display-line! "Change your particle's polarity with SPACE" 14)
      (display-line! "Don't touch any black obstacles" 28)
      (display-line! "Try to catch as many other particles as possible!" 42)
      (display-line! "Get the highest score possible!" 56)
      (sdl2:render-present! *renderer*)
      (sdl2:delay! 3000)

      (let/cc exit-main-loop!
              (while #t
                (set! (sdl2:render-draw-color *renderer*) +background-color+)
                (sdl2:render-clear! *renderer*)
                (particle-position-set! player (cdr (assoc 'player scene)))
                (particle-magnitude-set! player 2)
                (set! particles (fold
                                 (lambda (parameters all-particles)
                                   (cons (make-particle
                                          id: (format "particle-~S" (length all-particles))
                                          position: (car parameters)
                                          polarity: (cdr parameters))
                                         all-particles))
                                 '()
                                 (cdr (assoc 'particles scene))))
                (set! obstacle-speed (* -1 stage-num))
                (set! obstacle-num 5)
                (set! obstacle-min-gap (max 0 (- 100 (* (- stage-num 1) 10))))
                (set! obstacle-max-gap (max 60 (- 300 (* (- stage-num 1) 11))))
                (set! obstacles (list
                                 (make-random-obstacle "obstacle-1" 0 obstacle-min-gap obstacle-max-gap)
                                 (make-random-obstacle "obstacle-2" 300 obstacle-min-gap obstacle-max-gap)
                                 (make-random-obstacle "obstacle-3" 600 obstacle-min-gap obstacle-max-gap)))

                (display (format "min: ~S, max: ~S\n" obstacle-min-gap obstacle-max-gap))

                (if (let/cc exit-scene-loop!
                            (while #t

                              ;; move all obstacles to the left
                              (for-each (lambda (obstacle)
                                          (if (obstacle-enabled obstacle)
                                              (begin
                                                (obstacle-move! obstacle obstacle-speed)
                                                ;; Regenerate obstacles that have gone _completely_ off-screen
                                                (if (and (> (* -1 +obstacle-width+) (obstacle-position obstacle)))
                                                    (if (= 0 obstacle-num) (exit-scene-loop! #f)
                                                        (begin
                                                          (obstacle-randomize! obstacle 300 obstacle-min-gap obstacle-max-gap)
                                                          (set! obstacle-num (- obstacle-num 1)))))
                                                ;; Find out of any obstacles are touching the player
                                                (if (fold (lambda (rect intersects)
                                                            (or (sdl2:has-intersection?
                                                                 rect
                                                                 (particle-rect player))
                                                                intersects))
                                                          #f
                                                          (obstacle-rects obstacle))
                                                    (exit-scene-loop! #t)))))
                                        obstacles)

                              ;; move all particles in relation to player
                              (for-each (lambda (particle)
                                          (if (particle-enabled particle)
                                              (if (< (distance-to-player particle) (particle-mag-range player))
                                                  (let ((position (particle-position particle)))
                                                    (particle-move! particle
                                                                    (dx-to-player particle)
                                                                    (dy-to-player particle)))
                                                  (particle-move! particle obstacle-speed 0))))
                                        particles)

                              ;; detect collisions, off-window particles and absorb particles into player
                              (for-each (lambda (particle)
                                          ;; TODO: Avoid creating new rects here
                                          (if (sdl2:has-intersection? (particle-rect player) (particle-rect particle))
                                              ;; TODO: come up with particle types that decrease the player's magnitude
                                              (begin (particle-magnitude-inc! player)
                                                     (particle-disable! particle)))
                                          (if (or (> 0 (car (particle-position particle)))
                                                  (> 0 (cdr (particle-position particle)))
                                                  ;; Ignore particles that are ahead
                                                  (< +screen-height+ (cdr (particle-position particle))))
                                              (begin
                                                ;;(display (format "Particle ~S has exited the window\n" (particle-id particle)))
                                                (particle-disable! particle)))

                                          ;; Respawn disabled particles ahead
                                          (if (not (particle-enabled particle))
                                              (let ((new-position (cons +screen-width+
                                                                        (pseudo-random-integer +screen-height+))))
                                                ;; respawn
                                                ;;(display (format "Respawning particle ~S to ~S\n" (particle-id particle) new-position))
                                                (particle-position-set! particle new-position)
                                                (particle-polarity-set! particle (random-polarity))
                                                (particle-dirty-set! particle #t)
                                                (particle-enable! particle))))
                                        particles)

                              ;; Respawn particles
                              (for-each (lambda (particle) '())
                                        particles)

                              (let ((dx (cond ((member 'left pressed-keys) -5) ((member 'right pressed-keys) +5) (#t 0)))
                                    (dy (cond ((member 'up pressed-keys) -5) ((member 'down pressed-keys) +5) (#t 0))))
                                (particle-move! player dx dy))

                              (for-each (lambda (obstacle) (draw-obstacle! *renderer* obstacle))
                                        obstacles)

                              (draw-particles! *renderer* (cons player particles))

                              ;; Display the current point total
                              (display-text! (format "~S" game-points) #f)
                                 
                              ;; Handle all pending events.
                              (sdl2:pump-events!)
                              (while (sdl2:has-events?)
                                (handle-event (sdl2:poll-event! event) exit-scene-loop!))

                              ;; Present (i.e. show) any changes to the screen.
                              (sdl2:render-present! *renderer*)

                              ;; Pause briefly to let the CPU rest.
                              (sdl2:delay! 20)

                              ))
                    (begin
                      (display-text! (format "Sector failed... Points: ~S" game-points) #t)
                      (sdl2:render-present! *renderer*)
                        
                      (sdl2:delay! 2000)
                      (exit-main-loop! #f))

                    (begin
                      (set! stage-num (+ stage-num 1))
                      (set! game-points (+ game-points (* (particle-magnitude player) 1000)))
                      (display (format "Well done! Points: ~S\n" game-points))

                      (display-text! (format "Sector complete! Points: ~S" game-points) #t)
                      (sdl2:render-present! *renderer*)
                        
                      (sdl2:delay! 2000)
                      )
                    ) ;; If on scene loop

                ;; Set up the next scene
                                        ;(exit-main-loop! #t)
                ))                      ;; main-loop

      ;; Display the end game scene
      ) ;; let*

    (sdl2:quit!)
    ) ; main

  (main)
  )
