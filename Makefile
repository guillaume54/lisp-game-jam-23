CHICKEN ?= csc
TMPDIR ?= /tmp
OS = $(shell uname -s)
ARCH = $(shell uname -m)

.PHONY: default
default: trek

trek: trek.scm
	$(CHICKEN) -o trek trek.scm

.PHONY: static
static: trek.scm
	$(CHICKEN) -static -o trek -L -lSDL2 -L -lSDL2main -L -lSDL2_ttf trek.scm

.PHONY: package
package: static
	tar -czf magtrek-binary-$(OS)-$(ARCH).tar.gz DejaVuSans.ttf LICENSE trek

.PHONY: src-package
src-package: package
	cp -r . $(TMPDIR)/magtrek-src
	rm -rf $(TMPDIR)/magtrek-src/.git*
	tar -C $(TMPDIR) -czf magtrek-src.tar.gz magtrek-src
	rm -rf $(TMPDIR)/magtrek-src

run: trek
	./trek

deps:
	chicken-install sdl2 sdl2-ttf miscmacros defstruct srfi-1
